﻿using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace ImagicBotPoller
{
    class Program
    {
        private static TelegramBotClient _bot;

        private static HttpClient _httpClient;

        private static readonly string UnsplashToken = Environment.GetEnvironmentVariable("UNSPLASH_TOKEN");

        private static readonly string TelegramToken = Environment.GetEnvironmentVariable("TELEGRAM_TOKEN");

        static void Main(string[] args)
        {
            _bot = new TelegramBotClient(TelegramToken);

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://api.unsplash.com"),
                DefaultRequestHeaders =
                {
                    { "Authorization", $"Client-ID {UnsplashToken}" },
                },
            };

            _bot.OnMessage += BotOnMessage;

            _bot.StartReceiving();

            Console.WriteLine("Bot is active");

            Console.ReadLine();

            _bot.StopReceiving();
        }

        private static async void BotOnMessage(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if (message == null || message.Type != MessageType.TextMessage)
            {
                return;
            }

            var messageText = message.Text;

            if (!messageText.StartsWith("/show "))
            {
                return;
            }

            var keyword = messageText.Replace("/show ", "");

            await _bot.SendTextMessageAsync(message.Chat.Id, "Зацени");

            try
            {
                var photoRecordResponse = await _httpClient.GetAsync($"/photos/random?query='{keyword}'");

                var photoRecord = await photoRecordResponse.Content.ReadAsStringAsync();

                var photoRecordJson = JObject.Parse(photoRecord);

                var regularUrl = photoRecordJson["urls"]["regular"].Value<string>();

                await _bot.SendPhotoAsync(message.Chat.Id, new FileToSend(new Uri(regularUrl)));
            }
            catch (Exception e)
            {
                await _bot.SendTextMessageAsync(message.Chat.Id, "Не обессудь - ничего не нашел");
            }
        }
    }
}
